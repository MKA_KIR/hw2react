import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';
import {Header} from '../../../client/Header/Components/Header';
import {Main} from "../../../client/Main/components/Main";



export const App = () => {
    const pageProps ={
        header:{
            title:'Book',
            title2:'List',
            subtitle:'Add your book information to store it in database.'
        }
    };
    return (
        <div className="container">
            <Header {...pageProps.header}/>
            <Main />
        </div>
    );
};

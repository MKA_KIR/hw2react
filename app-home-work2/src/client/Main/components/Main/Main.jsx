import React, {Component} from 'react';
import {AddBookForm} from "../../../../shared/components/AddBookForm";
import {BooksInfo} from "../../../../shared/components/BooksInfo";
import {BookList} from "../../../../shared/components/BookList";
import './Main.css';

export class Main extends Component {

        state ={
            bookList: [{
                title:'Малазанская книга Падших',
                author:'Стивен Эриксон',
                isbn:'978-3-16-148410-0'
            }]
        };

        addBook = (title, author, isbn) =>{
            const newBook = {title, author, isbn};
            this.setState(({bookList}) =>{
                return {
                    bookList: [...bookList, newBook]
                }
            })
        };
        deleteBook = (index) => {
            this.setState(({bookList})=>{
                const newBookList = [...bookList];
                newBookList.splice(index, 1);
                return{
                    bookList: newBookList
                }
            })
        };
        updateBook = (index, title, author, isbn) =>{
            this.setState(({bookList})=>{
                const newBookList = [...bookList];
                newBookList[index] = {title, author, isbn};
                return{
                    bookList: newBookList
                }
            })
        };

    render() {
        const {bookList} = this.state;
        const count = bookList.length;
        return (
            <>
            <div className="row">
                <div className="col-lg-4">
                 <AddBookForm addBook ={this.addBook}/>
                </div>
            </div>
            <BooksInfo count={count}/>
        <BookList bookList={bookList} deleteBook={this.deleteBook} updateBook={this.updateBook}/>
        </>
        )
    }
}
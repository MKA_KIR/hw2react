import React from 'react';
import './Header.css';
export const Header = (props) =>{
    const {title, title2, subtitle} = props;
        return (
            <header className='header'>
                <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i><span className='title'>{title}</span>{title2}</h1>
                <p className='text-center'>{subtitle}</p>
            </header>
        )
};
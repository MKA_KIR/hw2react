import React from 'react';
import './Input.css';



export const Input = ({ name, handleChange, className, type, placeholder, required=false, value}) => {

    const fullClassName = (className) ? `form-control ${className}` : 'form-control';
    return (
        <input name={name} type={type} required={required} value={value} placeholder={placeholder} className={fullClassName} onChange={handleChange}/>
    );
};





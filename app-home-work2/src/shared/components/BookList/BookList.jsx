import React from 'react';
import {BookItem} from "../BookItem";
import './BookList.css';

export const BookList = ({bookList, deleteBook, updateBook}) =>{
        const bookItems = bookList.map((book, index) => <BookItem {...book}
                                                                  deleteBook={()=>deleteBook(index)}
                                                                  updateBook={(title, author, isbn)=>updateBook(index, title, author, isbn)}/>);
        return (
            <table className="table table-striped mt-2">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN#</th>
                </tr>
                </thead>
                <tbody>
                {bookItems}
                </tbody>
            </table>
        )
};
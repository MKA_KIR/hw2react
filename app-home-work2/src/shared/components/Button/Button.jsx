import React from 'react';
import './Button.css';

const buttonTypes = {
    primary: 'btn-primary',
    secondary: 'btn-secondary',
    third: 'btn-third'
};

export const Button = ({ text, type, handleClick}) => {
    return (
        <button onClick={handleClick} className={`btn ${buttonTypes[type]}`}>
            {text}
        </button>
    )
};


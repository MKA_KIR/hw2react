import React, {Component} from 'react';
import './AddBookForm.css';
import {Input} from "../Input";
import {Button} from "../Button";

export class AddBookForm extends Component{
        state ={
            title:'',
            author:'',
            isbn:''
        };

        handleChange = (event) =>{
           const obj = {[event.target.name]: event.target.value};
            this.setState(obj);
        };
        handleSubmit = (e) =>{
            e.preventDefault();
            const {title, author, isbn} = this.state;
            this.props.addBook(title, author, isbn);
            this.setState({title:'', author:'', isbn:''})
        };
    render() {
        const {title, author, isbn} = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <Input type="text" name="title" value={title} required handleChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <Input type="text" name="author" value={author} required handleChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="title">ISBN#</label>
                    <Input type="text" name="isbn" value={isbn} required handleChange={this.handleChange}/>
                </div>
                <Button type="primary" text="Add Book"/>
            </form>
        )
    }
}
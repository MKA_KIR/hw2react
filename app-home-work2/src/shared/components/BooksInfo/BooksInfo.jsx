import React from 'react';
import './BooksInfo.css';


export const BooksInfo = ({count}) => {

    return (
        <h3 id="book-count" className="book-count mt-5">Всего книг: {count}</h3>
    )
};
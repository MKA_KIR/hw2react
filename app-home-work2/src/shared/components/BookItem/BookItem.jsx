import React, {Component} from 'react';
import './BookItem.css';
import {Input} from "../Input";
import {Button} from "../Button";

export class BookItem extends Component{
        state ={
            edit: false,
            title:'',
            author:'',
            isbn:''
        };

        editBook = () =>{
            const {title, author, isbn} = this.props;
            this.setState({edit:true, title, author, isbn});
        };

        finishUpdate = (e) =>{
            e.preventDefault();
            const {title, author, isbn} = this.state;
            this.props.updateBook(title, author, isbn);
            this.setState({edit:false});
        };

         handleChange = (event) =>{
        const obj = {[event.target.name]: event.target.value};
        this.setState(obj);
         };

    render() {
        if (this.state.edit) {
            const {title, author, isbn} = this.state;
            return (
                    <tr>
                        <td>
                            <div className="form-group">
                                <Input type="text" name="title" value={title} required handleChange={this.handleChange}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <Input type="text" name="author" value={author} required handleChange={this.handleChange}/>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <Input type="text" name="isbn" value={isbn} required handleChange={this.handleChange}/>
                            </div>
                        </td>
                        <td><Button type="primary" text="Update" handleClick={this.finishUpdate}/>
                        </td>
                        <td></td>
                    </tr>
        )
        }
        else {
            const {title, author, isbn, deleteBook} = this.props;
            return (
                <tr>
                    <td>{title}</td>
                    <td>{author}</td>
                    <td>{isbn}</td>
                    <td><a href="#" className="btn btn-info btn-sm" onClick={this.editBook}><i className="fas fa-edit"></i></a></td>
                    <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={deleteBook}>X</a></td>
                </tr>
            )
        }
    }
}